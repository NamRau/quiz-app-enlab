import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PagesComponent } from './pages.component';
import { HomeQuizComponent } from './components/home-quiz/home-quiz.component';
import { RouterModule } from '@angular/router';
import { PagesRoutingModule } from './pages-routing.modules';
import {MatButtonModule} from '@angular/material/button';
import {MatInputModule} from '@angular/material/input';
import { QuestionComponent } from './components/question/question.component';
import { FormsModule } from '@angular/forms';
import {MatIconModule} from '@angular/material/icon';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    PagesRoutingModule,
    MatButtonModule,
    MatInputModule,
    FormsModule,
    MatIconModule
  ],
  declarations: [
    PagesComponent,
    HomeQuizComponent,
    QuestionComponent
  ]  
})
export class PagesModule { }

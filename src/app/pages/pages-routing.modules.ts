import { Route, RouterModule } from "@angular/router";
import { HomeQuizComponent } from "./components/home-quiz/home-quiz.component";
import { NgModule } from "@angular/core";
import { QuestionComponent } from "./components/question/question.component";

const routes: Route[] = [
    {path: 'home', component: HomeQuizComponent},
    {path: 'question', component: QuestionComponent},


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class PagesRoutingModule { }
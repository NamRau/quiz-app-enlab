/* tslint:disable:no-unused-variable */

import { TestBed, async, inject } from '@angular/core/testing';
import { QuizAppService } from './quiz-app.service';

describe('Service: QuizApp', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [QuizAppService]
    });
  });

  it('should ...', inject([QuizAppService], (service: QuizAppService) => {
    expect(service).toBeTruthy();
  }));
});

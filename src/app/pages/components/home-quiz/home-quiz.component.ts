import { Component, ElementRef, OnInit, ViewChild } from '@angular/core';

@Component({
  selector: 'app-home-quiz',
  templateUrl: './home-quiz.component.html',
  styleUrls: ['./home-quiz.component.scss']
})
export class HomeQuizComponent implements OnInit {

  @ViewChild('name') nameKey!: ElementRef;
  constructor() { }

  ngOnInit() {
  }

  public startQuiz(){
   localStorage.setItem("name",this.nameKey.nativeElement.value);
    
  }
}

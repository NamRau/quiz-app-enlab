import { Component, OnInit } from '@angular/core';
import { QuizAppService } from '../../services/quiz-app.service';
import { interval } from 'rxjs';

@Component({
  selector: 'app-question',
  templateUrl: './question.component.html',
  styleUrls: ['./question.component.scss']
})
export class QuestionComponent implements OnInit {
  public name : string = "";
  public questionList : any = [];
  public currentQuestion : number = 0;
  public points: number = 0;
  public interval$: any;
  public correctAnswer : number = 0;
  public incorrectAnswer : number = 0; 
  public counter: number = 60 ;
  public isQuizCompleted : boolean = false;
  constructor(
      private quizService : QuizAppService
  ) { }

  ngOnInit() {
    this.name = localStorage.getItem("name")!;
    this.getAllQuestion();
    this.startCounter();
  }
  
  public getAllQuestion(){
    this.quizService.getQuestionJson().subscribe(response =>{
      this.questionList = response.questions;
    })
  }

  public nextQuestion(){
    this.currentQuestion++;
  }

  public previousQuestion(){
    this.currentQuestion--;
  }

  public answer(currentQuestion: number, option:any){

    if(currentQuestion === this.questionList.length){
      this.isQuizCompleted = true;
      this.stopCounter();
    }
    if(option.correct){
      this.points+= 10;
      // this.points = this.points + 10;
      this.correctAnswer++;
      this.currentQuestion++;
    }else{
      this.currentQuestion++;
      this.incorrectAnswer++;
    }
  }

  public startCounter(){
    this.interval$ = interval(1000).subscribe(response =>{
      this.counter--;
      if(this.counter === 0){
        this.currentQuestion++;
        this.counter = 60;
        // this.points-=10; 
      }
    });
    setTimeout(() => {
      this.interval$.unsubscribe();
    },600000);
  }

  public stopCounter(){
    this.interval$.unsubscribe();
    this.counter = 0;
  }

  public resetCounter(){
    this.stopCounter();
    this.counter = 60;
    this.startCounter();
  }

  public resetQuiz(){
    this.resetCounter();
    this.getAllQuestion();
    this.points = 0;
    this.counter = 60;
  }
}

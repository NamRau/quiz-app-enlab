export const environment = {
    production: false,
    header: 'QuizApp',
    localization: {},
    HOME_PAGE: 'http://localhost:4200',
    apiBaseUrl: 'http://localhost:8080',
}